package group7;

public interface IShape3d {
    double getVolume();
    double getSurfaceArea();
}
