package group7;

/**
 * Sphere is a class that stores the radius of a sphere and gets its volume and 
 * surface area and implement the interface IShape3d
 * 
 * @author Emmanuelle Lin
 * @version 10/04/2023
*/
public class Sphere implements IShape3d{
    private double radius;

    /**
     * Constructor for the Sphere object
     * @param radius
     */
    public Sphere(double radius){
        
            this.radius = radius;
        if(this.radius < 0){
            throw new IllegalArgumentException("Radius shouldn't be negative");
        }
    }

    /**
     * Gets the private radius field
     * @return double radius
     */
    public double getRadius(){
        return this.radius;
    }
    
    /**
     * Gets the a sphere's volume with 4/3pi * radius^3
     * @return double volume
     */
    public double getVolume(){
        return Math.PI * Math.pow(getRadius(), 3);
    }
    
    /**
     * Gets the a sphere's surface area with 4pi * radius^2
     * @return double surfaceArea
     */
    public double getSurfaceArea(){
        return 4 * Math.PI * Math.pow(getRadius(), 2);
    }
}
