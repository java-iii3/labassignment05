package group7;

/**
 * Cylinder implements the IShape3d interface and stores a height and a radius
 * 
 * Author: You Ran Wang
 * Version 10/04/2024
 */
public class Cylinder implements IShape3d{
    private double height;
    private double radius;

    /**
     * Overide for constructor
     * @param height
     * @param radius
     */
    public Cylinder(double height, double radius){
        if(height < 0 || radius < 0){
            throw new IllegalArgumentException("Height and Radius cannot be negative!");
        }
        this.height = height;
        this.radius = radius;
    }

    /**
     * Getter method for height
     * @return double/height
     */
    public double getHeight(){
        return this.height;
    }

    /**
     * Getter method for radius
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * Gets the volume of the cylinder using height and radius
     */
    public double getVolume(){
        return Math.PI * Math.pow(this.radius, 2) * this.height;
    }

    /**
     * Gets the surface area of the cylinder using height and radius
     */
    public double getSurfaceArea(){
        return (2 * Math.PI * Math.pow(this.radius, 2) + 2 * Math.PI * this.radius * this.height);
    }
}
