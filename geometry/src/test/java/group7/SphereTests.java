package group7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Sphere tests are tests for the class sphere
 * 
 * Author: You Ran Wang
 * Version 10/08/2024
 */
public class SphereTests {

    /**
     * constructorTest which should fail
     */
    @Test
    public void constructorTestShouldFail(){
        try {
            Sphere bads = new Sphere(-2);
            fail("should not accept negative values");
        } catch (IllegalArgumentException e) {
            // yay
        }
    }

    /**
     * getTest which should return radius 2
     */
    @Test
    public void constructorandGetterTestShouldAnswer2(){
        Sphere s = new Sphere(2);
        assertEquals(2.0, s.getRadius(), 0.001);
    }

    /**
     * getVolume test which should return PI times 8
     */
    @Test
    public void getVolumeTestAnswers8xPI(){
        Sphere s = new Sphere(2);
        assertEquals(8*Math.PI, s.getVolume(), 0.000001);
    }

    /**
     * getSurfaceArea test which should return PI times 16
     */
    @Test
    public void getSurfaceAreaTestAnswers16xPI(){
        Sphere s = new Sphere(2);
        assertEquals(16*Math.PI, s.getSurfaceArea(), 0.000001);
    }
}
