package group7;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTests {


    @Test
    public void getHeight_return5()
    {
        Cylinder testCylinder = new Cylinder(5, 4);
        assertEquals(5, testCylinder.getHeight(), 0.0001);
    }

    @Test
    public void getRadius_return4()
    {
        Cylinder testCylinder = new Cylinder(5, 4);
        assertEquals(4, testCylinder.getRadius(), 0.0001);
    }

    @Test
    public void getVolume_returns_251_32()
    {
        Cylinder testCylinder = new Cylinder(5, 4);
        assertEquals(Math.PI * Math.pow(testCylinder.getRadius(), 2) * testCylinder.getHeight(), testCylinder.getVolume(), 0.0001);
    }
    
    @Test
    public void getSurfaceArea_returns_226_195()
    {
        Cylinder testCylinder = new Cylinder(5, 4);
        assertEquals(2*Math.PI * Math.pow(4, 2) + 2*Math.PI * 4 * 5, testCylinder.getSurfaceArea(), 0.0001);
    }
}
